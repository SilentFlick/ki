:- op(1100, yfx, or).
:- op(400, yfx, and).
:- op(200, fx, not).

negation(F,G) :-
    atom(F),
    G = F.
negation(not F,G) :-
    atom(F),
    G = not F.
negation(not (not F), G) :-
    negation(F,G).
negation(not(F or G), F1 and G1) :-
    negation(not F, F1),
    negation(not G, G1).
negation(not(F and G), F1 or G1) :-
    negation(not F, F1),
    negation(not G, G1).
negation(F and G, F1 and G1) :-
    negation(F,F1),
    negation(G,G1).
negation(F or G, F1 or G1) :-
    negation(F,F1),
    negation(G,G1).
