rew(A*(B*F),C*F) :- rew(A*B,C).
rew(A+(B+F),C+F) :- rew(A+B,C).
rew(A+(B-F),C-F) :- rew(A+B,C).
rew(A-(B+F),C+F) :- rew(A-B,C).
rew(A-(B-F),C-F) :- rew(A-B,C).
rew(A+B,C) :-
    number(A),
    number(B),
    C is A+B.
rew(A-B,C) :-
    number(A),
    number(B),
    C is A-B.
rew(A*B,C) :-
    number(A),
    number(B),
    C is A*B.
rew(0.0,0).
rew(-0.0,0).
rew(1.0,1).
rew(A-A,0).
rew(-A+A,0).
rew(A+A,2*A).
rew(0+A,A).
rew(A+0,A).
rew(0-A,-A).
rew(A-0,A).
rew(1*A,A).
rew(-1*A,-A).
rew(A*1,A).
rew(A*(-1),-A).
rew(0*_,0).
rew(_*0,0).
rew(A^1,A).
rew(A/1,A).
rew(A/(-1),-A).
rew(A/A,1).
rew(0/_,0).
rew(A,A).

simp(A+B,R) :-
    simp(A,SA),
    simp(B,SB),
    rew(SA+SB,R).
simp(A-B,R) :-
    simp(A,SA),
    simp(B,SB),
    rew(SA-SB,R).
simp(A*B,R) :-
    simp(A,SA),
    simp(B,SB),
    rew(SA*SB,R).
simp(A/B,R) :-
    simp(A,SA),
    simp(B,SB),
    rew(SA/SB,R).
simp(A,B) :- rew(A,B).

diff(X,X,1).
diff(C,X,0) :-
    atomic(C),
    C \== X.
diff(-F,X,-DF) :- diff(F,X,DF).
diff(C*F,X,C*DF) :-
    diff(C,X,0),
    diff(F,X,DF).
diff(F+G,X,DF+DG) :-
    diff(F,X,DF),
    diff(G,X,DG).
diff(F-G,X,DF-DG) :-
    diff(F,X,DF),
    diff(G,X,DG).
diff(F/G,X,(DF*G-F*DG)/(G^2)) :-
    diff(F,X,DF),
    diff(G,X,DG).
diff(F*G,X,DF*G+F*DG) :-
    diff(F,X,DF),
    diff(G,X,DG).
diff(X^N,X,N*X^M) :-
    atomic(X),
    number(N),
    M is N-1.
diff(F^N,X,N*DF*F^M) :-
    number(N),
    diff(F,X,DF),
    M is N-1.
diff(1/F,X,-DF/F^2) :-
    diff(F,X,DF).
diff(sin(F),X,DF*cos(F)) :- diff(F,X,DF).
diff(cos(F),X,-DF*sin(F)) :- diff(F,X,DF).
diff(exp(F),X,DF*exp(F)) :- diff(F,X,DF).

differenziert(F,X,DF) :-
    diff(F,X,DR),
    simp(DR,DF).
