:- [idDfs].

ort(X) :- member(X, [kleinschachwitz, pillnitz]).
goal([pillnitz,pillnitz,pillnitz,pillnitz]). %% [ziege,kohlkopf,wolf,faehrmann]
adj([ZF1,K,W,ZF1], [ZF2,K,W,ZF2]) :- ort(ZF1), ort(ZF2).
adj([Z,KF1,W,KF1], [Z,KF2,W,KF2]) :-
    ort(KF1), ort(KF2),
    Z \== W.
adj([Z,K,WF1,WF1], [Z,K,WF2,WF2]) :-
    ort(WF1), ort(WF2),
    Z \== K.
adj([Z,K,W,F1], [Z,K,W,F2]) :-
    ort(F1), ort(F2),
    Z \== K, Z \== W.
solution(P) :-
    idDfs([kleinschachwitz,kleinschachwitz,kleinschachwitz,kleinschachwitz],P),
    maplist(writeln,P).
