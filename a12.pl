:- [idas].
:- multifile h/2.

goal([[1,2,3],[4,5,6],[7,8,9]]).

sr([9,A,B], [A,9,B]).
sr([A,9,B], [A,B,9]).
shiftr(X,Y) :- sr(X,Y); sr(Y,X).

adjr([X, R2, R3], [Y, R2, R3]) :- shiftr(X, Y).
adjr([R1, X, R3], [R1, Y, R3]) :- shiftr(X, Y).
adjr([R1, R2, X], [R1, R2, Y]) :- shiftr(X, Y).

adjc([[A,B,C],[D,E,F],[G,H,I]], [[A,D,G],[B,E,H],[C,F,I]]).

adj(Board1, Board2) :-
    adjr(Board1, Board2);
    adjc(Board1, Board2).

h0(X,Y,0) :- X == Y.
h0(_,_,1).

h(Board,HD) :-
    flatten(Board,B),
    goal(GoalB), flatten(GoalB,G),
    maplist(h0, B, G, Diffs),
    sumlist(Diffs, HD), !.

f([H|T], Y) :-
    g([H|T], Y1),
    h(H, Y2),
    Y is Y1 + Y2.

g(L,Y) :-
    length(L, Y0),
    Y is Y0 - 1.

printB(Board) :-
    maplist(writeln, Board),
    writeln('').
print(Boards) :-
    maplist(printB, Boards).

solution(Board, Ret) :-
    idas(Board, Ret),
    print(Ret).

test(R) :- solution([[3,1,9],[4,2,5],[7,8,6]],R).

% indexOf([Element|_], Element, 0):- !.
% indexOf([_|Tail], Element, Index):-
%   indexOf(Tail, Element, Index1),
%   !,
%   Index is Index1+1.
