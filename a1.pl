%% eltern(Vater,Mutter,Kind,Geschlecht).
eltern(wilhelm,frieda,robert,m).
eltern(kurt,lisa,sandra,f).
eltern(winfried,elke,thomas,m).
eltern(hans,paula,christine,f).
eltern(robert,sandra,jens,m).
eltern(robert,sandra,jan,m).
eltern(robert,sandra,julia,f).
eltern(robert,sandra,jutta,f).
eltern(jan,eva,anne,f).
eltern(jan,eva,anke,m).
eltern(jens,heike,tim,m).

%% Ist Y ... von X.
elternR(X,Y) :-
    eltern(Y,_,X,_);
    eltern(_,Y,X,_).
elternR(X,Y) :-
    (eltern(Z,_,X,_); eltern(_,Z,X,_)),
    elternR(Z,Y).

bruder(X,Y) :-
    X \== Y,
    eltern(_,_,Y,m),
    eltern(_,_,X,_).

schwester(X,Y) :-
    X \== Y,
    eltern(_,_,Y,f),
    eltern(_,_,X,_).

enkel(X,Y) :-
    X \== Y,
    elternR(X,Y).

onkel(X,Y) :-
    X \== Y,
    eltern(_,_,X,_),
    bruder(_,Y).

tante(X,Y) :-
    X \== Y,
    eltern(_,_,X,_),
    schwester(_,Y).

vorfahr(X,Y) :-
    X \== Y,
    elternR(X,Y).
