:- [idas].

edge(a,b,90).
edge(a,c,110).
edge(b,z,10).
edge(c,z,1).

goal(z).

adj(X,Y) :-
    edge(X,Y,_);
    edge(Y,X,_).

f(X,Y) :-
    g(X, Y1),
    h(X, Y2),
    Y is Y1 + Y2.
f([H|T],Y) :-
    g([H|T], Y1),
    h(H, Y2),
    Y is Y1 + Y2.

h(a,105).
h(b,9).
h(c,1).
h(z,0).

g(X,Y) :-
    edge(Prev,X,Y1), g(Prev,Y2), Y is Y1 + Y2;
    edge(X,_,_), Y is 0.

g(L,Y) :- length(L, Y0), Y is Y0 - 1.
