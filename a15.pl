:- use_module(library(clpfd)).

color(NumColor, Ret) :-
    States = [BW, BY, BE, BB, HB, HH, HE, MV, NI, NW, RP, SL, SN, ST, SH, TH],
    States ins 1 .. NumColor,
    BW #\= BY, BW #\= HE, BW #\= RP,
    BY #\= SN, BY #\= TH, BY #\= HE,
    BE #\= BB,
    BB #\= SN, BB #\= ST, BB #\= MV, BB #\= NI,
    HB #\= NI,
    HH #\= NI, HH #\= SH,
    HE #\= NI, HE #\= TH, HE #\= RP, HE #\= NW,
    MV #\= NI, MV #\= SH,
    NI #\= ST, NI #\= TH, NI #\= NW, NI #\= SH,
    NW #\= RP,
    SL #\= RP,
    SN #\= ST, SN #\= TH,
    ST #\= TH,
    label(States),
    Ret = [[bw,BW], [by,BY], [be,BE], [bb, BB], [hb, HB], [hh, HH], [he, HE],
          [mv, MV], [ni, NI], [nw, NW], [rp, RP], [sl, SL], [sn, SN], [st,ST],
          [sh, SH], [th, TH]].
