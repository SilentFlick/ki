:- consult(a3).
:- consult(a4).

taylor(F,X,R) :-
    differenziert(F,X,DF),
    differenziert(DF,X,DFF),
    ev(F,0,RF),
    ev(DF,0,RDF),
    ev(DFF,0,RDFF),
    simp(RF+RDF*X+(RDFF*X^2/2),R).
