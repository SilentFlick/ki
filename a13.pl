:- [a12].
:- multifile h/2.

position(Index, Row, Col) :-
    Row is div(Index, 3) + 1,
    Col is mod(Index, 3) + 1.

manhattan(Board, Goal, MD) :-
    findall(D,
           (nth0(IB, Board, Tile),
           nth0(IG, Goal, Tile),
           Tile \= 9, % Don't calculate empty tile.
           position(IB, X1, Y1),
           position(IG, X2, Y2),
           D is abs(X1 - X2) + abs(Y1 - Y2)),
           Distances),
    sum_list(Distances, MD).

h(Board, MD) :-
    goal(GoalB),
    flatten(Board, FB),
    flatten(GoalB, FG),
    manhattan(FB, FG, MD).
