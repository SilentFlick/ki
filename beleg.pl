:- [idas].

goal([
    [1,2,3,4],
    [5,6,7,8],
    [9,10,11,12],
    [13,14,15,16]]).

compact_list(_, 16).
compact_list(L, E) :-
    not(member(E,L)).

hc([], [], 0).
hc([HB|TB], [HG|TG], R) :-
    forall(R = 2,
           (
               exclude(compact_list(HG), HB, HBC),
               hc1(HBC, HG, R);
               hc(TB, TG, R)
           )), R is 2;
    R is 0.

%% This version does not work properly.
% ordered(L) :- \+ ( append(_,[A,B|_], L), A > B ).
% hc([], [], 0).
% hc([HB|TB], [HG|TG], R) :-
%     exclude(compact_list(HG), HB, HBC),
%     ordered(HBC),
%     hc(TB, TG, R);
%     R is 2.

hc1([_], _, 0).
hc1([H|T], G, R) :-
    forall(R = 2,
           (
               member(X, T), hc2(H, X, G, R);
               hc1(T, G, R)
           )).

hc2(Q, P, G, R) :-
    Q > P,
    member(Q, G), member(P, G),
    R is 2.


position(Index, Row, Col) :-
    Row is div(Index, 4) + 1,
    Col is mod(Index, 4) + 1.

manhattan(Board, Goal, MD) :-
    findall(D,
            (nth0(IB, Board, Tile),
             nth0(IG, Goal, Tile),
             Tile \== 16,
             position(IB, X1, Y1),
             position(IG, X2, Y2),
             D is abs(X1 - X2) + abs(Y1 - Y2)),
            Distances),
    sum_list(Distances, MD).

h(Board, H) :-
    goal(GoalB),
    flatten(Board, FB),
    flatten(GoalB, FG),
    manhattan(FB, FG, HM),
    hc(Board, GoalB, HC),
    H is HM + HC.


g(L,Y) :-
    length(L, Y0),
    Y is Y0 - 1.

f([H|T], Y) :-
    g([H|T], Y1),
    h(H, Y2),
    Y is Y1 + Y2.


sr([16,A,B,C], [A,16,B,C]).
sr([A,16,B,C], [A,B,16,C]).
sr([A,B,16,C], [A,B,C,16]).
shiftr(X,Y) :- sr(X,Y); sr(Y,X).

adjr([X, R2, R3, R4], [Y, R2, R3, R4]) :- shiftr(X, Y).
adjr([R1, X, R3, R4], [R1, Y, R3, R4]) :- shiftr(X, Y).
adjr([R1, R2, X, R4], [R1, R2, Y, R4]) :- shiftr(X, Y).
adjr([R1, R2, R3, X], [R1, R2, R3, Y]) :- shiftr(X, Y).

adjc(Board1, Board2) :-
    flatten(Board1, B),
    nth0(Index, B, 16),
    position(Index, Row, Col),
    (Row1 is Row + 1;
    Row1 is Row - 1),
    Row1 >= 0, Row1 =< 4,
    verticalSwap(Board1, Row, Row1, Col, Board2).

verticalSwap(Board, Row1, Row2, Col, Board2) :-
    nth1(Row1, Board, R1),
    nth1(Row2, Board, R2),
    nth1(Col, R2, E2),
    select(16, R1, E2, NR1),
    select(E2, R2, 16, NR2),
    select(R1, Board, NR1, BoardT),
    select(R2, BoardT, NR2, Board2).

adj(Board1, Board2) :-
    adjr(Board1, Board2);
    adjc(Board1, Board2).

printB(Board) :-
    maplist(writeln, Board),
    writeln('').
printBs(Boards) :-
    maplist(printB, Boards).

solution(Board, Ret) :-
    idas(Board, Ret),
    printBs(Ret), !.

testboard1 :- solution([[1,2,3,4],[5,6,7,8],[9,10,11,12],[13,14,16,15]],_).
testboard2 :- solution([[16,1,2,4],[5,6,3,8],[9,10,7,12],[13,14,11,15]],_).
testboard3 :- solution([[5,1,2,4],[9,6,3,8],[13,10,7,12],[14,11,16,15]],_).
testboard4 :- solution([[5,1,2,4],[13,10,7,12],[9,6,3,8],[14,11,16,15]],_).
testboard5 :- solution([[12,16,15,3],[2,6,14,8],[10,13,11,5],[7,1,4,9]],_). %% 57 moves
