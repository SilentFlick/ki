%% replace x with value
ev(Var,Num,Num) :-
    atom(Var),
    number(Num).
%% if x is a number, then return x
ev(Num,_,Num) :- number(Num).

ev(A+B,Num,Y) :-
    ev(A,Num,SA),
    ev(B,Num,SB),
    Y is SA+SB.
ev(A-B,Num,Y) :-
    ev(A,Num,SA),
    ev(B,Num,SB),
    Y is SA-SB.
ev(A*B,Num,Y) :-
    ev(A,Num,SA),
    ev(B,Num,SB),
    Y is SA*SB.
ev(A/B,Num,Y) :-
    ev(A,Num,SA),
    ev(B,Num,SB),
    Y is SA/SB.
ev(A^B,Num,Y) :-
    ev(A,Num,SA),
    ev(B,Num,SB),
    Y is SA**SB.
ev(sin(x),0,0).
ev(sin(F),Num,Y) :-
    ev(F,Num,RF),
    Y is sin(RF).
ev(cos(x),0,1).
ev(cos(F),Num,Y) :-
    ev(F,Num,RF),
    Y is cos(RF).
ev(-F,Num,Y) :-
    ev(F,Num,RY),
    Y is -RY.
