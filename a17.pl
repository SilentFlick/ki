:- use_module(library(clpfd)).

merge(X, Y, [X,Y]).

placement(W, L, Ret) :-
    length(W, Lw),
    length(Ps, Lw),
    D #= L // 2, ND #= -D,
    Ps ins ND .. -1 \/ 1 .. D, %% Somehow -D .. -1 \/ 1 .. D returns error
    all_distinct(Ps),
    scalar_product(W, Ps, #=, 0),
    label(Ps), maplist(merge, W, Ps, Ret).
