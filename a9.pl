:- [idDfs].
valid([M,K,_]) :- %% [M,K,Boot_Position].
    (K =< M; M = 0),
    M2 is 3 - M, K2 is 3 - K,
    (K2 =< M2; M2 = 0).
move([M1,K,left], [M2,K,right]) :- %% 1M left -> right
    M1 > 0, M2 is M1 - 1.
move([M1,K,left], [M2,K,right]) :- %% 2M left -> right
    M1 > 0, M2 is M1 - 2.
move([M1,K1,left], [M2,K2,right]) :- %% 1M,1K left -> right
    M1 > 0, K1 > 0, M2 is M1 - 1, K2 is K1 - 1.
move([M,K1,left], [M,K2,right]) :- %% 1K left -> right
    K1 > 0, K2 is K1 - 1.
move([M,K1,left], [M,K2,right]) :- %% 2K left -> right
    K1 > 0, K2 is K1 - 2.
move([M1,K,right], [M2,K,left]) :- %% 1M left <- right
    M1 < 3, M2 is M1 + 1.
move([M1,K,right], [M2,K,left]) :- %% 2M left <- right
    M1 < 2, M2 is M1 + 2.
move([M1,K1,right], [M2,K2,left]) :- %% 1M,1K left <- right
    M1 < 3, K1 < 3, M2 is M1 + 1, K2 is K1 + 1.
move([M,K1,right], [M,K2,left]) :- %% 1K left <- right
    K1 < 3, K2 is K1 + 1.
move([M,K1,right], [M,K2,left]) :- %% 2K left <- right
    K1 < 2, K2 is K1 + 2.
adj([M1,K1,P1], [M2,K2,P2]) :-
    move([M1,K1,P1], [M2,K2,P2]),
    valid([M2,K2,P2]).
goal([0,0,right]).
solution(P) :- idDfs([3,3,left],P), maplist(writeln, P).
