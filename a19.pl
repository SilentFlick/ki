:- use_module(library(clpfd)).

timetable(TB) :-
    Ls = [[lec1, prof1, sem1, T1, 1], [lec2, porf2, sem2, T2, 1],
          [lec3, prof3, sem3, T3, 1], [lec4, prof4, sem4, T4, 1],
          [lec5, prof5, sem5, T5, 1], [lec6, prof6, sem6, T6, 2],
          [lec7, prof7, sem7, T7, 2], [lec8, prof8, sem8, T8, 2],
          [lec9, prof9, sem9, T9, 2], [lec10, prof10, sem10, T10, 2]],
    maplist(in, [T1, T2, T3, T4, T5], [8, 10, 12, 14, 16]),
    maplist(in, [T6, T7, T8, T9, T10], [8, 10, 12, 14, 16]),
    TB = Ls.
