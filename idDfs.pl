dfs(Node, Depth, Path, Ret) :-
    goal(Node), reverse(Path, Ret);
    Depth > 0,
    adj(Node, NewNeighbor), not(member(NewNeighbor,Path)),
    NewDepth is Depth - 1,
    dfs(NewNeighbor, NewDepth, [NewNeighbor|Path], Ret).

iddfs_helper(Node, Depth, Path) :-
    dfs(Node, Depth, [Node], Path);
    NewDepth is Depth + 1,
    iddfs_helper(Node, NewDepth, Path).

idDfs(Node,Path) :-
    iddfs_helper(Node, 0, Path).
