list([9,8,9,7,9,8,1,2,9]).

count([], Path, R) :- flatten(Path, R).
count(List, Path, R) :-
    length(List, L),
    nth0(0, List, H),
    delete(List, H, NList),
    length(NList, NL),
    C is L - NL,
    count(NList, [H-C|Path], R).
