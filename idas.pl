idas(Start, Ret) :-
    f([Start], Depth),
    idas_helper(Start, Depth, Ret).

idas_helper(Start, Depth, Ret) :-
    as(Start, Depth, [Start], Ret);
    NewDepth is Depth + 1,
    idas_helper(Start, NewDepth, Ret).

as(Node, Depth, Path, Ret) :-
    goal(Node), reverse(Path, Ret);
    adj(Node, NewNeighbor), not(member(NewNeighbor, Path)),
    f([NewNeighbor|Path], CDepth), CDepth =< Depth,
    as(NewNeighbor, Depth, [NewNeighbor|Path], Ret).
