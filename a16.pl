:- use_module(library(clpfd)).

category([vorspeise, hauptspeise, nachtisch]).

dish(vorspeise, kokssuppe, 400).
dish(vorspeise, fruehlingsrollen, 167).
dish(vorspeise, sommerrollen, 350).
dish(vorspeise, tomatensupper, 300).
dish(vorspeise, tomatensalat, 150).
dish(hauptspeise, campagnola, 700).
dish(hauptspeise, capricciosa, 710).
dish(hauptspeise, rustika, 690).
dish(hauptspeise, siciliana, 750).
dish(nachtisch, solero, 150).
dish(nachtisch, veganes_mango, 143).
dish(nachtisch, avocado_tiramisu, 332).
dish(nachtisch, quarkcreme, 300).

%% menu([], Selected, Cmax, Ret) :-
%%     length(Selected, L),
%%     L #> 0, Cmax #>= 0,
%%     Ret = Selected.
%% menu([H|T], Selected, Cmax, Ret) :-
%%     Cmax #>= 0,
%%     findall([Dm,Cm], dish(H, Dm, Cm), Ds),
%%     member([Dish,Cal], Ds),
%%     Cmax #= Cal + NewCmax,
%%     NewCmax #>= 0,
%%     menu(T, [[H,Dish,Cal]|Selected],NewCmax, Ret).

menu([V,H,N], Cmax, Ret) :-
    dish(V, SV, CV),
    dish(H, SH, CH),
    dish(N, SN, CN),
    Cmax #>= CV + CH + CN,
    Ret = [SV,  SH, SN].

solution(Cmax, Ret) :-
    category(Cat),
    menu(Cat, Cmax, Ret).
