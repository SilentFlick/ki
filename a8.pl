:- [idDfs].
adj(X,Y) :-
    adj0(X,Y);
    adj0(Y,X).
adj0(X,Y) :-
    member((X,Y),
        [(1,2), (2,3), (3,4), (2,7), (7,8),
        (7,12), (12,11), (11,6), (11,16), (16,17),
        (16,21), (17,22), (22,23), (23,24), (24,25),
        (23,18), (18,13), (25,20), (20,19), (20,15),
        (19,14), (14,13), (14,9), (9,10), (10,5)]).

goal(5). %% defines the goal node
solution(P) :- idDfs(1,P).
